# grunt-hash-extended

> Powerful hash | cache busting plugin for grunt

## Table of content
[TOC]

## Getting Started
This plugin requires Grunt `~0.4.0`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-hash-extended --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-hash-extended');
```

or by using the [load-grunt-tasks](https://www.npmjs.com/package/load-grunt-tasks)




## Documentation
_Run this task with the `grunt hash-extended` command._


### Settings

There are a number of options available:

```js
{
    files: <file>
    references:[
        {
            cwd: <cwd>
            src: ['<globbing>','<pattern>']
            baseRefDir: '<base directory the references will be calculated from>'
            requirejs: true|false
        }
    ]
    options:
    {
        hashAlgorithm    : 'md5',
        hashLength       : 8,
        hashPrefix       : 'zhn'

        renameFile       : true,
        removeSource     : false,

        onComplete       : null,
        encoding         : 'utf8',

        fileMap          : null
    }
}
```

### files
Type: [Files Array Format](http://gruntjs.com/configuring-tasks#files-array-format)

Those are the files you want to **HASH**

### references
Type: Custom Files Array Format

Those files are the files you want to have **references updated**.
Those should not contain any binary files.

```js
    references:[
        {
            cwd: <cwd>
            src: ['<globbing>','<pattern>']
            baseRefDir: '<base directory the references will be calculated from>'
        }
    ]
```  
* **cwd**  
mandatory  
as for normal File Array Format  

* **src**  
mandatory  
as for normal File Array Format

* **baseRefDir**  
optional  
the reference base path.  
This is useful for instance when you have backend files hosted outside your served folder, like ```myproject/views/*.twig``` and the served folder being for instance ```myproject/web```. The references in the _.twig_ files should be based on the ```myproject/web``` path. In this case, _baseRefDir_ would be ```myproject/web```

* **requirejs**
optional, default to false
If this is true, the regular expression to look for reference will omit the extensions. so for instance the regex
```/('|")(my/path/my/script.min.js)(.*?)['|"]/g``` will become ```/('|")(my/path/my/script.min)(.*?)['|"]/g```

### options.hashAlgorithm
Type: `String`  
Default: 'md5'  

This defines what algorithms to be used to generate the hash.
It is dependent on the available algorithms supported by the version of OpenSSL on the platform.  
Examples are 'sha1', 'md5', 'sha256', 'sha512', etc.  
_see [NodeJS Crypto library](https://nodejs.org/api/crypto.html#crypto_crypto_createhash_algorithm) for more information_

### options.hashLength
Type: `Integer`  
Default: 8

This defines the length of the hash.

### options.hashPrefix
Type: `String`  
Default: 'zhn'

This defines a hash prefix that will be appended to the hash itself. This is to make it simpler for some specific use cases (see examples bellow)

### options.renameFile
Type: `Boolean`  
Default: true

This defines if the hash files should be renamed or not, using the following logic ```<filename>-<hashPrefix><hash>.<ext>```

### options.removeSource
Type: `Boolean`  
Default: false

This defines if the hashed files (just renamed/copied) sources' should be removed.

### options.onComplete
Type: `Function`  
Default: null

This defines a callback function to be called when the task is completed. This helps you to handle the hash map, so you can write the output to json or sth.  
```function(map, files)``` where:  

* ```map``` is an object where the keys are the file sources and the values are the file destinations  
* ```files``` is an array of the file sources

### options.encoding
Type: `String`  
Default: 'utf8'

This defines the encoding used when writing / copying the files

### options.encoding
Type: `String`  
Default: 'utf8'

This defines the encoding used when writing / copying the files

### options.fileMap
Type: `String`  
Default: null

This defines a file where the map json object will be saved.  
By default (null) the map won't be saved.

## Examples
In the following example we will **NOT** rename the files but just the references of those files.
It means that our application will try to download files named ```<filename>-zhn<hash>.<ext>```.
To cater for this you will have to add a rule on your server to rewrite it to the file, for instance in Apache with mod_rewrite enabled:
```
RewriteRule (^.*)(-zhn.+?)\.(.+)$ $1.$3 [L]
```

The grunt task will look like this
```js
/* global module,require */
module.exports = function (grunt) {
    'use strict';

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        // Project settings
        application: {
            'src'    : 'app',
            'dist'   : '../../dist/apps/login'
        },

        'hash-extended' : {
            all : {
                files : [
                    {
                        expand : true,
                        cwd    : '<%= application.dist %>/web',
                        src    : [
                            '**/*.js',
                            '**/*.css',
                            '**/*.{eot,svg,ttf,woff}',
                            '**/*.{png,jpg}'],
                        dest   : '<%= application.dist %>/web'
                    }
                ],

                references: [
                    {
                        cwd    : '<%= application.dist %>/web',
                        src    : [
                            '**/*.js',
                            '**/*.css'
                        ]
                    },
                    {
                        cwd         : '<%= application.dist %>/views',
                        src         : [ '**/*.twig' ],
                        baseRefDir  : '<%= application.dist %>/web'
                    }
                ],

                options : {
                    //  the hash algorithm
                    hashAlgorithm   : 'md5',
                    //  the hash length
                    hashLength      : 8,
                    //  the hash prefix
                    hashPrefix      : 'zhn'

                    //  do we want to rename the files
                    renameFile: false,
                    // should we removed the source files?
                    removeSource : false,

                    // function to handle the hash map
                    // so you can write the output to json or sth ;-)
                    // in this case, we write json AND php output of our map :-)
                    onComplete  : function ( map, files ) {
                        var arr = [];

                        files.forEach( function ( file ) {
                            arr.push( '\'' + file + '\'=>\'' + map[ file ] + '\'' );
                        });

                        var out = '<?php\n\n$files = [\n\t' + arr.join( ',\n\t' ) + '\n];\n';

                        //grunt.file.write( 'test/dist/map.php', out );
                        grunt.file.write( application.dist + '/map.json', JSON.stringify( map ) );
                    }
                }
            }
        }
    }
}
```
### 

## Release History
 * 2015-10-12   v0.1.6   Path and regex OS independent
 * 2015-09-29   v0.1.5   Removed unnecessary logs
 * 2015-09-29   v0.1.4   Improved regex
 * 2015-09-29   v0.1.3   Fixed error when no references passed
 * 2015-09-29   v0.1.2   Fixed own reference throwing error.
 * 2015-09-29   v0.1.1   Updated regex
 * 2015-09-13   v0.1.0   Updated regex, fixed no map when referencing only.
 * 2015-03-13   v0.0.10   Fixed looping issue.
 * 2015-03-13   v0.0.9
      * adding RequireJS support and updated documentation and options
      * updated the documentation to have the rewrite rule working with minified extensions
 * 2015-03-12   v0.0.8   Fixed path generation on windows OS ('\\' replaced with '/')
 * 2015-03-12   v0.0.7   Fixed regex generation on windows OS ('\\' replaced with '/')
 * 2015-03-12   v0.0.6   Forgot to bump the version number in documentation
 * 2015-03-12   v0.0.5   Fixed unwanted dependency
 * 2015-03-12   v0.0.4   Fixed options variable name not following documentation
 * 2015-03-11   v0.0.3   Updating documentation
 * 2015-03-11   v0.0.2   Updating documentation
 * 2015-03-11   v0.0.1   Initial release