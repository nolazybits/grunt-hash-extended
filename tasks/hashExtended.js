module.exports = function(grunt) {
    'use strict';

    var path    = require('path'),
        crypto  = require('crypto');

    grunt.registerMultiTask( 'hash-extended', 'Powerful grunt cache-busting / hash plugin', function () {
        //  set default options
        var options = this.options( {
            hashAlgorithm    : 'md5',
            hashLength       : 8,
            hashPrefix       : 'zhn',
            //hashFunction     : null,

            renameFile       : true,
            //renameReference  : true,
            removeSource     : false,

            //noProcess        : false,
            onComplete       : null,
            //refPathTransform : null,
            encoding         : 'utf8',

            fileMap          : null,
            fileMapFunction  : null
        });

        var processedFileList   = {};
        var referenceMap        = {};
        var refFileList         = {};
        var hashFileList        = {};
        var map                 = {};

        function separatorToUnix(string)
        {
            return string.split(path.sep).join('/');
        }

        function separatorToOs(string)
        {
            return string.split('/').join(path.sep)
        }

        function forEachFilename ( files, callback ) {
            files.forEach( function ( file ) {
                file.src.forEach( function ( src ) {
                    if ( ! grunt.file.exists( src ) ) {
                        grunt.verbose.writeln( 'source file "' + src + '" not found' );
                        return false;
                    }

                    if ( grunt.file.isDir( src ) ) {
                        grunt.verbose.writeln( 'source file "' + src + '" seems to be a directory' );
                        return false;
                    }

                    var filename = path.relative( file.orig.cwd, src );
                    callback( separatorToUnix(src), separatorToUnix(filename), separatorToUnix(file.dest), separatorToUnix(file.orig.cwd) );
                });
            });
        }

        function isFunc ( ref ) {
            return ref && typeof ref === 'function';
        }

        function replace ( source, file, regex ) {
            return source.replace( regex, function ( match, openquot, name, end, closequot ) {
                grunt.verbose.writeln( '    reference found: ' + match  );
                grunt.verbose.writeln( '    reference file name: ' + name  );

                var ext      = path.extname( name );
                var realName = path.join( path.dirname( name ), path.basename( name, ext )).replace(/\\/g, "/");

                grunt.verbose.writeln( '    change to : ' +  openquot + realName + '-' + file.hash + ext + end + closequot);
                return openquot + realName + '-' + file.hash + ext + end + closequot;
            });
        }

        function canBeProcessed( fileSrc) {
            if ( ! referenceMap[ fileSrc ] || ! referenceMap[ fileSrc ].length ) {
                return true;
            }

            var nonHashedCount = 0;
            referenceMap[ fileSrc ].forEach( function ( refFileSrc ) {
                if ( ! hashFileList[ refFileSrc.src ].hash && refFileSrc.src !== fileSrc ) {
                    nonHashedCount++;
                }
            });

            return nonHashedCount === 0;
        }

        function process( files ) {
            var fileKeys = Object.keys( files );
            if ( ! files || ! fileKeys.length) {
                return;
            }

            //  if the file is only a referencing file, or if it can be cached
            var restFiles = {};
            fileKeys.forEach( function ( fileSrc ) {
                var file = files[ fileSrc ];

                grunt.verbose.writeln('');
                grunt.verbose.writeln('Working on file: '+file.src);

                //  check if all the references in this file havent been processed already,
                //  if so, we can process this file
                if ( canBeProcessed( file.src ) ) {
                    if( processedFileList[ file.src] )
                    {
                        return;
                    }

                    //  the file source
                    var source = grunt.file.read( file.src, { encoding : options.encoding }),
                    //  where the source is going to be written
                        fileDest    = file.dest ? file.dest : file.src,
                    //  replace all the references with their hash
                        hasOwnReference = null;

                    processedFileList[ file.src ] = true;

                    if( referenceMap[ file.src ] )
                    {
                        grunt.verbose.writeln( ' Updating reference' );
                        referenceMap[ file.src ].forEach( function ( refFileSrc ) {
                            // handle own references differently to avoid infinite loops
                            if ( fileSrc !== refFileSrc.src ) {
                                grunt.verbose.writeln( '  Reference: ' + refFileSrc.src );
                                source = replace( source, hashFileList[ refFileSrc.src ], refFileSrc.regex );
                            } else {
                                hasOwnReference = refFileSrc;
                            }
                        });
                    }

                    //  if we want to hash the file
                    if( hashFileList[file.src] ) {

                        // hash file after references have been updated (except own references)
                        grunt.verbose.writeln( ' Hashing file' );
                        hashFileList[ file.src ].hash = options.hashPrefix + crypto
                                .createHash( options.hashAlgorithm )
                                .update( source, options.encoding )
                                .digest( 'hex' )
                                .substr( 0, options.hashLength );
                        //  define the hash destination filename
                        var fileDestSplit = fileDest.split( '.' );
                        if ( fileDestSplit.length >= 2 ) {
                            fileDestSplit[ fileDestSplit.length - 2 ] += '-' + hashFileList[ file.src ].hash;
                        }
                        hashFileList[ file.src ].destHashed = fileDestSplit.join( '.' );
                        grunt.verbose.writeln( ' Hash is ' + hashFileList[ file.src ].hash );

                        if( hasOwnReference ) {
                            // replace own reference with hash
                            grunt.verbose.writeln( ' Found own reference' + hasOwnReference.src);
                            source = replace( source, hashFileList[ hasOwnReference.src ], hasOwnReference.regex);
                        }

                        if( options.renameFile )
                        {
                            fileDest = fileDestSplit.join( '.' );
                        }

                        //  creating the mapping object
                        var dest =  separatorToUnix(path.relative( file.cwd, file.dest) || path.relative( file.cwd, file.src ));
                        map[ dest ] = JSON.parse(JSON.stringify(hashFileList[ file.src ]));
                        map[ dest ].src = separatorToUnix(path.relative( file.cwd, hashFileList[ file.src ].src ));
                        map[ dest ].dest = separatorToUnix(path.relative( file.cwd, hashFileList[ file.src ].dest ));
                        map[ dest ].destHashed = separatorToUnix(path.relative( file.cwd, hashFileList[ file.src ].destHashed ));
                    }

                    //  check if path to write is different from current path
                    if( file.src === fileDest ) {
                        if (refFileList[file.src])
                        {
                            grunt.file.write(
                                separatorToOs(fileDest),
                                source,
                                {encoding : options.encoding}
                            );
                        }
                    } else {
                        grunt.file.copy( separatorToOs(file.src), separatorToOs(fileDest), {
                            encoding : options.encoding,
                            process : function () {
                                // return source with updated references
                                return source;
                            },
                            // skip those files for updating refs - handy for libs
                            noProcess : options.noProcess
                        });
                    }
                } else {
                    grunt.verbose.writeln( ' Storing file: ' + file.src );
                    restFiles[ file.src ] = file;
                }
            });

            var restFilesKeys = Object.keys( restFiles );
            if (  restFilesKeys.length === fileKeys.length ) {
                restFilesKeys.forEach( function ( fileSrc ) {
                    restFiles[fileSrc].references = [];
                    referenceMap[ fileSrc ].forEach( function ( refFileSrc ) {
                        if ( ! hashFileList[ refFileSrc.src ].hash && refFileSrc.src !== fileSrc ) {
                            restFiles[fileSrc].references.push({
                                url: refFileSrc.src,
                                hash: hashFileList[ refFileSrc.src ]
                            });
                        }
                    });
                });
                grunt.fail.warn( 'Infinite loop while bushcasting ' + JSON.stringify( restFiles ) );
            }

            if ( Object.keys( restFiles ).length ) {
                grunt.verbose.writeln('Processing RestFiles');
                grunt.verbose.writeln(JSON.stringify(restFiles) );
                process( restFiles );
            }
        }

        // Files to be hashed and to look for reference in the reference file list
        forEachFilename( this.files, function ( src, filename, dest, cwd ) {
            hashFileList[ src ] = {
                src        : src,
                filename   : filename,
                dest       : dest,
                destHashed : undefined,
                cwd        : cwd,
                regex      : undefined,
                hash       : undefined
            };
        });

        //  create the expanded files for the references
        if( this.data.references )
        {
            this.data.references.forEach( function(files) {
                var refFiles  = grunt.file.expand({cwd: files.cwd, filter: files.filter}, files.src);

                refFiles.forEach( function ( src ) {
                    var joinedPath = separatorToUnix(path.join(files.cwd, src)),
                        baseRefDir = joinedPath;

                    if (files.baseRefDir) {
                        baseRefDir = path.join( files.baseRefDir, 'mock');
                    }

                    refFileList[ joinedPath ] = {
                        src         : joinedPath,
                        dest        : separatorToUnix(path.join( (files.dest ? files.dest : files.cwd), src)),
                        cwd         : separatorToUnix(files.cwd),
                        baseRefDir  : separatorToUnix(baseRefDir),
                        requirejs  : (files.hasOwnProperty('requirejs')?files.requirejs:false)
                    };
                });
            });
        }

        // generate reference map
        grunt.verbose.subhead( 'Building reference list' );

        // build reference list
        if( refFileList )
        {
            Object.keys( refFileList ).forEach( function ( fileSrc ) {
                // read file source to check for references in
                var source = grunt.file.read( separatorToOs(fileSrc), { encoding : options.encoding }),
                    baseRefDir = refFileList[fileSrc].baseRefDir || refFileList[fileSrc].src;

                referenceMap[ fileSrc ] = [];

                // and scan for references
                grunt.verbose.writeln('');
                grunt.verbose.writeln( 'Searching for references in ' + fileSrc );

                Object.keys( hashFileList ).forEach( function ( refFileSrc ) {
                    var basename = path.basename(hashFileList[ refFileSrc ].src);
                    if( refFileList[fileSrc].hasOwnProperty('requirejs') && refFileList[fileSrc].requirejs && path.extname(hashFileList[ refFileSrc ].src) === '.js' )
                    {
                        basename = path.basename(hashFileList[ refFileSrc ].src, path.extname(hashFileList[ refFileSrc ].src));
                    }
                    var regex = separatorToUnix(path.join(
                        path.relative(
                            path.dirname(baseRefDir),
                            path.dirname(hashFileList[ refFileSrc ].src)
                        ),
                        basename
                    )).replace(/[.?*+^$[\]\\\/(){}|-]/g, "\\$&");
                    regex = new RegExp( '((?:\'|"){1,2})((?:\.{0,2}\/)?' + regex + ')(.*?)((?:\'|"){1,2})', 'g' );

                    grunt.verbose.writeln( ' Looking for regex ' + regex );
                    if ( regex && source.match( regex ) ) {
                        grunt.verbose.writeln( '   Found reference ' + refFileSrc );
                        referenceMap[ fileSrc ].push( {src:refFileSrc, regex: regex, requirejs: refFileList[fileSrc].requirejs} );
                    }
                });
            });
        }

        // generate hashes
        grunt.verbose.subhead( 'Hashing files and copying/updating references' );
        grunt.verbose.writeln( JSON.stringify(hashFileList) );
        process( hashFileList );
        grunt.verbose.writeln( 'Processing refFileList' );
        grunt.verbose.writeln( JSON.stringify(refFileList) );
        process( refFileList );

        // remove source files for hashes, if desired
        if ( options.removeSource ) {
            grunt.verbose.subhead( 'deleting source files' );

            forEachFilename( this.files, function ( src ) {
                grunt.file.delete( src );
            });
        }

        //  writing map file
        if (options.fileMap || options.fileMapFunction) {
            grunt.verbose.writeln('Generated mapping: ' + options.fileMap);
            var output = JSON.stringify(map, null, "  ");
            if( options.fileMap ) {
                grunt.file.write(options.fileMap, output);

            }

            //  TOTO have custom function for anything we haven't think about
            if(options.fileMapFunction && isFunc(options.fileMapFunction)) {

            }
        }

        // do something with the map of hashes
        if ( isFunc( options.onComplete ) ) {
            grunt.verbose.subhead( 'executing onComplete' );
            options.onComplete( map, Object.keys( map ) );
        }
    });
};
